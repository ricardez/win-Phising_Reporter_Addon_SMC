﻿
$ErrorActionPreference = 'Stop';
$toolsDir   = "$(Split-Path -parent $MyInvocation.MyCommand.Definition)"
$fileLocation = Join-Path $toolsDir 'SMC PhishReporter.bat'
$url        = ''
$url64      = ''

$packageArgs = @{
  packageName   = $env:ChocolateyPackageName
  unzipLocation = $toolsDir
  fileType      = 'EXE'
  url           = $url
  url64bit      = $url64
  file         = $fileLocation

  softwareName  = 'win-Phising_Reporter_Addon_SMC*'

  checksum      = '398552B028D8DB117BEEC2A12311C67A19841A03DE34E137ABF380C04A24DBDE'
  checksumType  = 'sha256'
  checksum64    = '398552B028D8DB117BEEC2A12311C67A19841A03DE34E137ABF380C04A24DBDE'
  checksumType64= 'sha256'

  
  validExitCodes= @(0, 3010, 1641)
  silentArgs   = ''
}

Install-ChocolateyInstallPackage @packageArgs










    








